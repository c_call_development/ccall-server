/**
 * ScheduleController
 *
 * @description :: Server-side logic for managing schedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var sender = require('../utils/NotificationSender')();

module.exports = {
  create: function (req, res) {
    var messageData = req.body.data;
    var scheduleData;
    if (typeof messageData === 'string') {
      scheduleData = JSON.parse(messageData);
    } else {
      scheduleData = messageData;
    }
    var date = new Date(scheduleData.scheduledTime);
    console.log(date);
    Schedule.create(scheduleData).then(function (created) {
      scheduleData.serverId = created.id.toString();
      scheduleData.id = 0;
      var response = sender.send(scheduleData, scheduleData.receiver, 3);
      console.log(response);
      return res.json({serverId: scheduleData.serverId});
    });
  },
  accept: function (req, res) {
    console.log(req.body);
    Schedule.find({id: req.body.scheduleId}).then(function (results) {
        if (results.length > 0) {
          var scheduledData = results[0];
          console.log(scheduledData);
          scheduledData.isAccepted = true;
          console.log(scheduledData.scheduledTime);
          console.log(new Date(scheduledData.scheduledTime));
          Schedule.update({id: req.body.scheduleId}, scheduledData).exec(function(updated){
            scheduledData.serverId = scheduledData.id.toString();
            scheduledData.id = 0;
            sender.send(scheduledData, scheduledData.sender,4);
          });
        }
      }
    );
    return res.json("ok");
  },
  reject: function (req, res) {
    console.log("reject was called");
    Schedule.find({id: req.body.scheduleId}).then(function (results) {
      if (results.length > 0) {
        var scheduledData = results[0];
        scheduledData.serverId = scheduledData.id.toString();
        scheduledData.id = 0;
        sender.send(scheduledData, scheduledData.sender,5);
      }
    });
    return res.json("ok");
  }
};

