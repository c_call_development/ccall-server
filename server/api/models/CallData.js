/**
* CallData.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    imageId: {type: 'string'},
    message: {type: 'string'},
    receiver: {type: 'string'},
    sender: {type: 'string'},
    urgency: {type: 'integer'},
    time: {type: 'datetime'}
  }
};

