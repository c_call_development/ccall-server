/**
 * AppUserController
 *
 * @description :: Server-side logic for managing appusers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var crypto = require('crypto');
var request = require('request');
var _ = require('underscore.string');
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

var createCode = function(){
  var text = "";
  var possible = "0123456789";
  for (var i = 0; i < 6; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var guid = function(){
  var result = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = crypto.randomBytes(1)[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
  return result;
};

var clearLocationFromNumber = function(number){
  console.log(number);
  var phoneNumber = phoneUtil.parse(number);
  //console.log(phoneNumber);
  return phoneNumber.values_['1'].toString() + phoneNumber.values_['2'].toString();
};

module.exports = {
  index: function(req, res) {
    AppUser.find().where({phoneNumber: req.body.phoneNumber})
      .then(function (found) {
        if (found.length == 0) {
          AppUser.create(req.body)
            .then(function (created) {
              console.log(created);
              return res.json(created);
            });
        } else {
          console.log('update is needed');
          AppUser.update({phoneNumber: req.body.phoneNumber}, req.body).then(function (updated) {
            console.log(updated);
            return res.json(updated);
          });
        }
      })
  },

  verify: function(req, res){
    console.log("verify called");
    console.log("verification requested for " + req.body.phoneNumber);
    var phoneNumber = req.body.phoneNumber;
    //if (!(_.startsWith(phoneNumber,"+972"))){
    //  console.log("error");
    //  return res.json("error");
    //}
    var data = {phoneNumber: req.body.phoneNumber, verCode: createCode(), sessionId: guid()};
    NumberVerify.findOne().where({phoneNumber: req.body.phoneNumber}).then(
      function(result) {
        if (result) {
          NumberVerify.destroy({phoneNumber: req.body.phoneNumber, verCode: result.verCode}).then(function () {
            createAndSend(data);
          });
        } else {
          createAndSend(data);
        }
      });

    var createAndSend = function(data) {
      NumberVerify.create(data).then(function (created) {
        //Your c-call verification number is:
        // &msg=test%20a%20message&number=972547007160
        var url = "https://secure.annatel.net/sendsmswaik.php?from=WAIK";
        url += "&msg=Your waik verification code is:" + created.verCode;
        url += "&number=" + clearLocationFromNumber(created.phoneNumber);
        url = encodeURI(url);
        console.log(url);
        request(url, function(error, response, body){
        });
        console.log(created.sessionId);
        return res.json(created.sessionId);
      });
    }
  },

  verifyCheck: function(req, res){
    var number = req.body.phoneNumber;
    var code = req.body.code;
    var session = req.body.sessionId;
    console.log(session);
    NumberVerify.findOne().where({phoneNumber: number, verCode:code, sessionId:session}).then(function(result){
      if (result){
        return res.json(true);
      } else {
        return res.json(false);
      }
    })
  }
};

