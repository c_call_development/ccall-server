/**
 * ContactController
 *
 * @description :: Server-side logic for managing contacts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var _ = require('underscore');
var Q = require('q');
var sugar = require('sugar');

function fixNumber(owner) {
  if (owner && !owner.startsWith('+')){
      owner = '+' + owner;
      owner = owner.remove(' ');
    }
  return owner;
}

module.exports = {
	bulk: function(req, res) {
    console.log('bulk upload contacts');
    var promises = [];
    var itemsToSave = [];
    _.each(req.body.list, function(value, key, list) {
      value['ownerNumber'] = req.body.owner;
      promises.push(Contact.find().where({phoneNumber: value['phoneNumber'], 'ownerNumber': req.body.owner})
        .then(function(found, err){
          if(!found || found.length == 0){
            itemsToSave.push(value);
          }
        }));
    });

    Q.allSettled(promises).then(function(){
      Contact.create(itemsToSave)
        .then(function(){
          successCreatedContact(req.body.list);
        });
    });




    function successCreatedContact(createdContact){
      var users = [];
      var promises = [];
      for (var index in createdContact) {
        promises.push(AppUser.find().where({phoneNumber:createdContact[index].phoneNumber}).then(function(res){
          if (res && res.length > 0){
            users.push(res[0]);
          }
        }));
      }
      Q.allSettled(promises).then(function(){
        var response = {'success': true, 'items': []};
        var signedContacts = [];
        _.each(users, function(value){
          _.each(createdContact, function(contact){
            if (contact['phoneNumber'] === value['phoneNumber']){
              contact.isRegistered = true;
            }
          });
          //signedContacts.push({phoneNumber: value['phoneNumber']});
        });
        response.items = createdContact;
        return res.json(response);
      });
    }
  },

  check:function(req, res){
    var number = req.param('number');
    number = fixNumber(number.toString());
    AppUser.findOne().where({phoneNumber: number}).then(function(result){
      if (result){
        return res.json(true);
      } else {
        return res.json(false);
      }
    });
  },

  checkinstall: function(req, res){
    var numbers = JSON.parse(req.param('params'));
    var owner = req.param('owner');
    owner = fixNumber(owner);
    var promises = [];
    var results = [];
    _.each(numbers, function(value){
      value = fixNumber(value);
      promises.push(AppUser.find().where({phoneNumber: value})
        .then(function(found){
          if (found.length > 0){
                  results.push(found);
          }
        }));
    });
    Q.allSettled(promises).then(function(){
      console.log(results);
      results = _.uniq(results, function(item){
        return item.phoneNumber;
      });
      var resList = [];
      var contactPromises = [];
      _.each(results, function(value){
        console.log(value[0].phoneNumber);
        console.log(owner);
        contactPromises.push(Contact.find().where({phoneNumber: value[0].phoneNumber, ownerNumber: owner})
          .then(function(found){
            console.log(found);
            if (found.length > 0){
              resList.push({phoneNumber: found[0].phoneNumber, displayName: found[0].displayName});
            }
          }));
      });
      Q.allSettled(contactPromises).then(function(){
        console.log(resList);
        return res.json(resList);
      });
    });
  },

  installed: function(req, res){
    var owner = req.param('owner');
    owner = fixNumber(owner);
    console.log('hello world');
    console.log(owner);
    var promises = [];
    var results = [];
    Contact.find().where({ownerNumber: owner})
      .then(function (found, err){
      if (found && found.length > 0){
        console.log(found.length);
        _.each(found, function(value, key, list){
          console.log(value['phoneNumber']);
          promises.push(AppUser.find().where({phoneNumber: value['phoneNumber']})
            .then(function(found, err){
              if(found.length > 0){
                results.push(value);
              }
            }));
        });
      }
    }).then(function(){
      Q.allSettled(promises).then(function(){
        results = _.uniq(results, function(item){
          return item.phoneNumber;
        });
        var resList = [];
        _.each(results, function(value){
          console.log(value);
          resList.push({displayName: value.displayName, phoneNumber: value.phoneNumber});
        });
        console.log(resList);
        return res.json(resList);
      });
    });
  }
};

