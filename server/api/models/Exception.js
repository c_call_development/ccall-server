/**
* Exception.js
*
* @description :: This is the model for exception that happened on user device. We are catching all exceptions and sending the data to our server.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    brand: {type: 'string'},
    device: {type: 'string'},
    model: {type: 'string'},
    buildId: {type: 'string'},
    product: {type: 'string'},
    sdk: {type: 'string'},
    appVersion: {type: 'string'},
    phoneNumber: {type: 'string'},
    message: {type: 'string'},
    stack: {type: 'string'}
  }
};

