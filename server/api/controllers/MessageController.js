/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var gcm = require('node-gcm');
var sender = require('../utils/NotificationSender')();

var getMessage = function(data, dataName){
  var message = new gcm.Message();
  message.addData(dataName, JSON.stringify(data));
  return message;
};

var texts = {
  en: {
    normal: "NORMAL",
    important: "IMPORTANT",
    urgent: "URGENT",
    priorityText: '\nPriority: ',
    message: 'Message: ',
    attached: 'attached - Press open to see it \ue03b\n(or press close and answer the phone)',
    close: '(Press close and answer the call)'
  },
  fr: {
    normal: "NORMAL",
    important: "IMPORTANT",
    urgent: "URGENT",
    priorityText: '\nPriorité: ',
    message: 'Message: ',
    attached: 'Image ci-jointe - Appuyez sur Ouvrir pour voir  \ue03b\n(ou appuyez sur Fermer et répondez)',
    close: '(Appuyez sur Fermer et répondez)'
  }
};

var getMessageWithUrgency = function(phoneNumber, message, urgency, hasImage, lang){
  var currentLang = texts.en;
  if (lang && lang === 'fr'){
    currentLang = texts.fr;
  }

  var urgencyText = currentLang.normal;
  if (urgency == 1) {
    urgencyText = currentLang.important;
  } else if (urgency == 2){
    urgencyText = currentLang.urgent;
  }
  var resultMessage = phoneNumber + currentLang.priorityText + urgencyText + '\n';
  if (message){
    resultMessage = resultMessage + currentLang.message + message + '\n';
  }
  if (hasImage){
    resultMessage = resultMessage + currentLang.attached;
  } else {
    resultMessage = resultMessage + currentLang.close;
  }

  return resultMessage;
};


module.exports = {
  index: function(req, res){
    console.log('index');
    return res.view();
  },
  calldata: function (req, res) {
    console.log(req.body);
    var target = req.body.target;
    var messageData = req.body.data;
    var callData;
    if (typeof messageData === 'string') {
      callData = JSON.parse(messageData);
    } else {
      callData = messageData;
    }
    if (!callData.time){
      console.log("error");
      return res.json("error");
    }

    var token =
    console.log('message data');
    console.log(messageData);
    var response = sender.send(messageData, target, 1, function(){
      setTimeout(function(){
        console.log('message is ' + callData);
        console.log('message data is ' + callData.message);
        AppUser.find().where({phoneNumber: target}).then(function(res){
          var messageText = getMessageWithUrgency(callData.sender ,callData.message, callData.urgency, callData.imageId, res[0].lang);
          var notification = new gcm.Message({
            notification:{
              title:'Waik',
              body: messageText,
              sound: 'default',
              icon: 'default',
              data: messageData
            },
            priority:'high'
          });
          sender.sendIphoneNotification(notification, target);
        });
      }, 8900);
    }).then(function(response){
      CallData.create(callData).then(function(created){
        console.log(created);
        response.messageId = created.id;
      }).then(function(){
        console.log(response);
        return res.json(response);
      });
    });
  },

  uploadimage: function(req, res){
    console.log("upload image");
    console.log(typeof req.body);
    console.log(typeof req.body.photo);
    Image.create(req.body).then(function(image){
      return res.json("ok");
    });
  },

  image: function(req, res){
    console.log(req.param('id'));
    console.log(typeof req.param('id'));
    Image.findOne().where({photoId: req.param('id')}).then(function (result) {
      if (!result){
        console.log("error - Can't find picture");
        return res.json("error");
      }
      return res.json({photoId: result.photoId, photo: result.photo});
    })
  },
  img: function(req, res){
    Image.findOne().where({photoId: req.param('id')}).then(function (result) {
      if (!result){
        console.log("error - Can't find picture");
        return res.json("error");
      }
      return res.json({photoId: result.photoId, photo: base64ArrayBuffer(result.photo)});
    })
  },
  show: function(req, res){
    console.log('show');
    var callDataId = req.param('id');
    CallData.find({id:callDataId}).then(function(results){
      if (results){
        var result = results[0];
        if (!result.imageId){
          return res.json(result);
        }
        Image.find({photoId: result.imageId}).then(function(images){
          if (images){
            result.image =  base64ArrayBuffer(images[0].photo);
            return res.json(result);
          }
        })
      }
    });

        //return res.json();
  }
};



function base64ArrayBuffer(arrayBuffer) {
  var base64    = '';
  var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

  var bytes         = new Uint8Array(arrayBuffer);
  var byteLength    = bytes.byteLength;
  var byteRemainder = byteLength % 3;
  var mainLength    = byteLength - byteRemainder;

  var a, b, c, d;
  var chunk;

  // Main loop deals with bytes in chunks of 3
  for (var i = 0; i < mainLength; i = i + 3) {
    // Combine the three bytes into a single integer
    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

    // Use bitmasks to extract 6-bit segments from the triplet
    a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
    b = (chunk & 258048)   >> 12; // 258048   = (2^6 - 1) << 12
    c = (chunk & 4032)     >>  6; // 4032     = (2^6 - 1) << 6
    d = chunk & 63;               // 63       = 2^6 - 1

    // Convert the raw binary segments to the appropriate ASCII encoding
    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
  }

  // Deal with the remaining bytes and padding
  if (byteRemainder == 1) {
    chunk = bytes[mainLength];

    a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

    // Set the 4 least significant bits to zero
    b = (chunk & 3)   << 4; // 3   = 2^2 - 1

    base64 += encodings[a] + encodings[b] + '=='
  } else if (byteRemainder == 2) {
    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

    a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
    b = (chunk & 1008)  >>  4; // 1008  = (2^6 - 1) << 4

    // Set the 2 least significant bits to zero
    c = (chunk & 15)    <<  2; // 15    = 2^4 - 1

    base64 += encodings[a] + encodings[b] + encodings[c] + '='
  }

  return base64
}
