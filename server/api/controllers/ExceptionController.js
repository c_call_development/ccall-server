/**
 * ExceptionController
 *
 * @description :: Server-side logic for managing exceptions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  add: function(req, res){
    console.log(req.body);
    Exception.create(req.body).then(function(created){
      return res.json(created);
    });
  }
};

