/**
 * DeveloperController
 *
 * @description :: Server-side logic for managing developers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getAddress: function(req, res){
    console.log('address request');
    ServerData.find().then(function(result){
      return res.json({address: result[0].address});
    });
  },
  ping: function(req, res){
    return res.json("true");
  }
};

