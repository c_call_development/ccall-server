var gcm = require('node-gcm');
var Q = require('q');

var getMessage = function(data, dataName){
  console.log(data);
  var message = new gcm.Message();
  var jsonData =  JSON.stringify(data);
  console.log(jsonData);
  message.addData(dataName,jsonData);
  return message;
};

module.exports = function(){
  return {
    send: function (data, target, eventType, onIphoneSuccess) {
      var deferred = Q.defer();
      var sendData = {'eventType': eventType, 'data': data};
      var message = getMessage(sendData, 'message');
      var regTokens = [];
      var isIphoneUser = false;
      AppUser.find().where({phoneNumber: target}).then(function (res) {
        if (res.length > 0) {
          regTokens.push(res[0].token);
          if (res[0].system === 'IOS') {
            isIphoneUser = true;
          }
        } else {
          deferred.resolve({isTargetRegistered: false});
        }
      }).then(function () {
        console.log(regTokens);
        var sender = new gcm.Sender('AIzaSyAZGfbmviwegNjMJuX7UkP8cGx7zDYu6TQ');
        sender.send(message, {registrationTokens: regTokens}, function (err, response) {
          if (err) {
            console.error(err);
            deferred.resolve({isTargetRegistered: true});
          } else {
            console.log(response);
            if (isIphoneUser && onIphoneSuccess) {
              onIphoneSuccess();
            }
            deferred.resolve({isTargetRegistered: true});
          }
        });
      });
      return deferred.promise;
    },
    sendIphoneNotification: function (notification, target) {
      console.log('sending Iphone notification');
      var regTokens = [];
      AppUser.find().where({phoneNumber: target}).then(function (res) {
        if (res.length > 0) {
          regTokens.push(res[0].token);
        }
      }).then(function(){
        var sender = new gcm.Sender('AIzaSyAZGfbmviwegNjMJuX7UkP8cGx7zDYu6TQ');
        sender.send(notification, {registrationTokens: regTokens}, function(err, response){
          if (err){
            console.log(err);
          } else {
            console.log(response);
          }
        });
      });
    }
  };
};
