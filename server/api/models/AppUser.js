/**
* AppUser.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    phoneNumber: {type: 'string'},
    system: {type: 'string'},
    version: {type: 'string'},
    versionNum: {type: 'integer'},
    token: {type: 'string'}
  }
};

