/**
* Schedule.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    scheduledTime: {type: 'datetime'},
    urgency: {type: 'integer'},
    description: {type: 'string'},
    receiver: {type: 'string'},
    sender: {type: 'string'}
  }
};

